from django.shortcuts import render
from django.http import HttpResponse
import requests
import tmdbsimple as tmdb
import json 
import os


tmdb_key = 'ce68330397e5f801d51112fdcf2e14fc'
base_url = 'https://api.themoviedb.org/3/'
tmdb.API_KEY = 'ce68330397e5f801d51112fdcf2e14fc'
genres_url = 'https://api.themoviedb.org/3/genre/movie/list?api_key=ce68330397e5f801d51112fdcf2e14fc&language=en-US'

with open('movies.json') as f:
  data = json.load(f)


genreList = [
    "Action",
    "Adventure",
    "Fantasy",
    "Sci-Fi",
    "Thriller",
    "Romance",
    "Animation",
    "Family",
    "Comedy",
    "Musical",
    "Mystery",
    "Western",
    "Drama",
    "History",
    "War"
]

def matchInData(title):
    for i in data:
        if i['movieName'] == title['original_title']:
            return i

def findByGenre(query):
    movies = []
    for i in data:
        for g in i['genres']:
            if g == query:
                movies.append(i)
    return movies

            


#example: https://api.themoviedb.org/3/movie/550?api_key=ce68330397e5f801d51112fdcf2e14fc
def findById(id):
    url = f'{base_url}movie/{id}?api_key={tmdb_key}'
    response = requests.get(url)
    return response.json()


# Create your views here.
def home(request):
    return render(request, 'movifindr/home.html')

def about(request):
    return render(request, 'movifindr/about.html')

def movie(request, id):
    
    url = f'{base_url}movie/{id}?api_key={tmdb_key}'
    movie = requests.get(url).json()['original_title']
    movie = matchInData(tmdb.Movies(id).info())
    if movie is not None:
        tweetdata = [
        {
            'positive': movie['positiveReviews'],
            'negative': movie['negativeReviews'],
        }
        ]

    else:
        tweetdata = [
        {
            'positive': 0,
            'negative': 0,
        },
        ]
    context={
       'movie': tmdb.Movies(id).info(),
       'tweet_data': tweetdata,
    }
    return render(request, 'movifindr/movie.html', context)

def search(request):
    query = request.GET.get('query')
    search = tmdb.Search()
    response = search.movie(query=query)
    context={
       'search_results': search.results,
    }
    return render(request, 'movifindr/searchresult.html', context)

def extract_time(json):
    try:
        # Also convert to int since update_time will be string.  When comparing
        # strings, "10" is smaller than "2".
        return int(json['positiveReviews'])
    except KeyError:
        return 0


def genre(request):
    query = request.GET.get('Genre')
    movies = findByGenre(query)
    movies.sort(key=extract_time, reverse=True)
    context = {
        'genres': genreList,
        'query': query,
        'movies': movies,
    }
    return render(request, 'movifindr/genre.html', context)

def genre_selection(request):
    selected = request.GET['dropdown'] 
