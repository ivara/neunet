from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns = [
    path('', views.home, name='movifindr-home'),
    path('about/', views.about, name='movifindr-about'),
    path('movie/<int:id>/', views.movie, name='movifindr-movie'),
    url(r'^search/$', views.search, name='movifindr-search'),
    path('genre/', views.genre, name='movifindr-genre'),
    url(r'^genre/$', views.genre, name='movifindr-genreresult'),
]