from django.apps import AppConfig


class MovifindrCodeConfig(AppConfig):
    name = 'movifindr'
