Za pokretanje aplikacije prvo je potrebno pokrenuti virutal environment, iz WEBAPP foldera:
1. source bin/activate

Zatim je potrebno pokrenuti server, iz movifindr foldera unutar WEBAPP 
2. cd movifindr
3. python manage.py runserver


Server se pokreće na http://localhost:8000/
Podaci o pozitivnim/negativnim recenzijama uzimaju se iz movies.json datoteke u WEBAPP/movifindr koja se originalno generira u PPIJ_JSONFORMAT  