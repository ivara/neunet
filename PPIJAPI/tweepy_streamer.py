from tweepy import API
from tweepy import Cursor
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import re
import io
import csv

import twitter_credentials
import numpy as np
import pandas as pd
import csv
from collections import defaultdict
import re


# # # # TWITTER CLIENT # # # #
class TwitterClient():
    def __init__(self, twitter_user=None):
        self.auth = TwitterAuthenticator().authenticate_twitter_app()
        self.twitter_client = API(self.auth,wait_on_rate_limit=True,
				   wait_on_rate_limit_notify=True)

        self.twitter_user = twitter_user

    def get_twitter_client_api(self):
        return self.twitter_client

    def get_user_timeline_tweets(self, num_tweets):
        tweets = []
        for tweet in Cursor(self.twitter_client.user_timeline, id=self.twitter_user).items(num_tweets):
            tweets.append(tweet)
        return tweets

    def get_friend_list(self, num_friends):
        friend_list = []
        for friend in Cursor(self.twitter_client.friends, id=self.twitter_user).items(num_friends):
            friend_list.append(friend)
        return friend_list

    def get_home_timeline_tweets(self, num_tweets):
        home_timeline_tweets = []
        for tweet in Cursor(self.twitter_client.home_timeline, id=self.twitter_user).items(num_tweets):
            home_timeline_tweets.append(tweet)
        return home_timeline_tweets


# # # # TWITTER AUTHENTICATER # # # #
class TwitterAuthenticator():
    def authenticate_twitter_app(self):
        auth = OAuthHandler(twitter_credentials.CONSUMER_KEY, twitter_credentials.CONSUMER_SECRET)
        auth.set_access_token(twitter_credentials.ACCESS_TOKEN, twitter_credentials.ACCESS_TOKEN_SECRET)
        return auth


# # # # TWITTER STREAMER # # # #
class TwitterStreamer():
    """
    Class for streaming and processing live tweets.
    """

    def __init__(self):
        self.twitter_autenticator = TwitterAuthenticator()

    def stream_tweets(self, fetched_tweets_filename, hash_tag_list):
        # This handles Twitter authetification and the connection to Twitter Streaming API
        listener = TwitterListener(fetched_tweets_filename)
        auth = self.twitter_autenticator.authenticate_twitter_app()
        stream = Stream(auth, listener)

        # This line filter Twitter Streams to capture data by the keywords:
        stream.filter(track=hash_tag_list)


# # # # TWITTER STREAM LISTENER # # # #
class TwitterListener(StreamListener):
    """
    This is a basic listener that just prints received tweets to stdout.
    """

    def __init__(self, fetched_tweets_filename):
        self.fetched_tweets_filename = fetched_tweets_filename

    def on_data(self, data):
        try:
            print(data)
            with open(self.fetched_tweets_filename, 'a') as tf:
                tf.write(data)
            return True
        except BaseException as e:
            print("Error on_data %s" % str(e))
        return True

    def on_error(self, status):
        if status == 420:
            # Returning False on_data method in case rate limit occurs.
            return False
        print(status)


class TweetAnalyzer():
    """
    Functionality for analyzing and categorizing content from tweets.
    """

    def tweets_to_data_frame(self, tweets):
        df = pd.DataFrame(data=[tweet.text for tweet in tweets], columns=['Tweets'])

        return df






def get_tweets(query, count = 300):

    # empty list to store parsed tweets
    tweets = []
    target = io.open("mytweets.txt", 'a', encoding='utf-8')
    # call twitter api to fetch tweets
    fetched_tweets = api.search(query +" movie", count = count) + api.search("movie " + query, count=count)
    # parsing tweets one by one
    print(len(fetched_tweets))

    for tweet in fetched_tweets:

        # empty dictionary to store required params of a tweet
        parsed_tweet = {}
        # saving text of tweet
        parsed_tweet['text'] = tweet.text
        if "http" not in tweet.text and tweet.lang == 'en':
            line = re.sub("[^A-Za-z]", " ", tweet.text)
            line = line + '|' + query
            line = line.lstrip('RT')
            line = re.sub("[\n]", " ", line)
            print(line + "\n ----------------------------------------------------------------")
            target.write(line+"\n")
    return tweets


if __name__ == '__main__':
    twitter_client = TwitterClient()
    tweet_analyzer = TweetAnalyzer()

    columns = defaultdict(list)  # each value in each column is appended to a list

    with open('movie_metadata.csv') as f:
        reader = csv.DictReader(f)  # read rows into a dictionary format
        for row in reader:  # read a row as {column1: value1, column2: value2,...}
            for (k, v) in row.items():  # go over each column name and value
                columns[k].append(v)  # append the value into the appropriate list
                # based on column name k

    columns['movie_title'] = [x.strip() for x in columns['movie_title']]
    columns['movie_title'] = [x.strip('\xa0') for x in columns['movie_title']]


    api = twitter_client.get_twitter_client_api()
    if (not api):
        print("Cant AUTHENTICATE")
    for i  in range(40):
        tweets = get_tweets(query=columns['movie_title'][i+200], count=50)

    lines_seen = set()  # holds lines already seen
    outfile = open("mytweetsFormatted.txt", "a")
    for line in open("mytweets.txt", "r"):
        if line not in lines_seen:# not a duplicate
            lines_seen.add(line)
            line = line.lstrip()
            line = line.rstrip()
            line += "\n"
            outfile.write(line)

    open('mytweets.txt', 'w').close()
    outfile.close()


    # print(dir(tweets[0]))
    # print(tweets[0].retweet_count)

