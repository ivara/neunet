import MovieGlobals


# A class which contains data about how a set of words is categorized and counted.
class WordsFormat:

    # Constructor, takes number of variables.
    def __init__(this, n):
        this.categories = [[] for i in range(n)]

    # Sets the words for the given variable to the given list of words.
    def SetWords(this, variable, words):
        this.categories[variable] = words

    # Adds the given word to the list of words for a given variable.
    def AddWord(this, variable, word):
        this.categories[variable].append(word)

    # Finds the given word in lists for all variables, and increments counters in the given array for all found.
    def MatchWord(this, array, word):
        for i in range(len(this.categories)):
            for known in this.categories[i]:
                if word == known:
                    if MovieGlobals.USE_VARIABLE_AS_BOOLEAN:
                        array[i] = 1
                    else:
                        array[i] += 1

    # Returns the number of variables
    def GetN(this):
        return len(this.categories)

# A class which contains data about how many words of each category are in a string (variable array)
class WordsData:

    # Constructor. Takes the WordsFormat object and a string from which to construct the data on.
    def __init__(this, wFormat, text, answer):
        words = text.split(" ")
        this.format = wFormat
        this.decision = answer
        this.values = [0 for i in range(wFormat.GetN())]
        for word in words:
            wFormat.MatchWord(this.values, word)
        if MovieGlobals.EXTENDED_DEBUG_ALLOWED:
            print(",".join([str("".join(wFormat.categories[i])) + "_" + str(this.values[i]) for i in range(len(this.values))]) + " _ " + this.decision)
        




