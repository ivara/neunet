from MovieData import *
from IdNode import *
from IdApi import IdDecisionForest
import MovieGlobals
import random




def main():
    random.seed()
    astrTrainingFiles = ["tp1.txt","tp4.txt","tp3.txt"]
    astrTestFiles = ["mytweetsFormatted.txt"]

    astrTweets = []
    astrTitles = []
    astrDecisions = []
    for strTestFile in astrTestFiles:
        file = open(strTestFile, "r")
        astrLines = file.read().split("\n")
        for strLine in astrLines:
            if len(strLine) == 0:
                continue
            astrParts = strLine.split("|")
            astrTweets.append(astrParts[0])
            astrTitles.append(astrParts[1])

    idForest = IdDecisionForest()
    idForest.Fit(astrTrainingFiles, MovieGlobals.FOREST_SIZE, MovieGlobals.MAX_DEPTH)
    astrDecisions, afConf = idForest.Predict(astrTweets, MovieGlobals.CONFIDENCE_SHARPNESS)


    fullTweets = []
    for i in range(len(astrDecisions)):
        fullTweets.append (astrTweets[i] + "|" + astrTitles[i] + "|" +astrDecisions[i])

    outfile = open("mytweetsFormattedWithDecisions.txt", "w")

    for i in fullTweets:
        i = i + "\n"
        outfile.write(i)

if __name__ == "__main__":
    main()
