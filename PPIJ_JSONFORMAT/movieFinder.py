import csv
from collections import defaultdict
import re

columns = defaultdict(list) # each value in each column is appended to a list

with open('movie_metadata.csv') as f:
    reader = csv.DictReader(f) # read rows into a dictionary format
    for row in reader: # read a row as {column1: value1, column2: value2,...}
        for (k,v) in row.items(): # go over each column name and value
            columns[k].append(v) # append the value into the appropriate list
                                 # based on column name k

columns['movie_title'] = [x.strip() for x in columns['movie_title']]
columns['movie_title'] = [x.strip('\xa0') for x in columns['movie_title']]


tweet = "transformers age of extinction is not a good movie at all"
movieGenres = {}

for i in range(len(columns['movie_title'])):
    movieGenres[columns['movie_title'][i]] = columns['genres'][i]

tweetWords = tweet.split()
namebuilder = [None] * len(tweetWords)
for i in range(len(tweetWords)):
    namebuilder[i] = tweetWords[i]

print(namebuilder)

listOfFindings =[]
for i in columns['movie_title']:
    j = str(i).lower()
    withoutSpecialCharacters = re.sub('[-:!,.]', '', j)
    for k in range(len(tweetWords)):
        watchedString = str(namebuilder[k]).lower()
        if ((watchedString.__eq__(j) or watchedString.__eq__(withoutSpecialCharacters))and i not in listOfFindings):
            print("FOUND " + i)
            listOfFindings.append(i)

        for z in range(len(tweetWords)):
            if (z < len(tweetWords) - 1 and z >= k):
                namebuilder[k] = namebuilder[k] + ' ' + tweetWords[z + 1]

                watchedString = str(namebuilder[k]).lower()
                if ((watchedString.__eq__(j) or watchedString.__eq__(withoutSpecialCharacters)) and i not in listOfFindings):
                    print("FOUND " + i)
                    listOfFindings.append(i)

    for r in range(len(tweetWords)):
        watchedString = str(namebuilder[r]).lower()
        if ((watchedString.__eq__(j) or watchedString.__eq__(withoutSpecialCharacters)) and i not in listOfFindings):
            print("FOUND " + i)
            listOfFindings.append(i)
        namebuilder[r] = tweetWords[r]

chosenMovie = ""
maxLenght = 0
for movie in listOfFindings:
    if(maxLenght < len(movie)):
        chosenMovie = movie
        maxLenght = len(movie)




print(movieGenres)