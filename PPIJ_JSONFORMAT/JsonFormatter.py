import csv
from collections import defaultdict
import re
import json

columns = defaultdict(list) # each value in each column is appended to a list

with open('movie_metadata.csv') as f:
    reader = csv.DictReader(f) # read rows into a dictionary format
    for row in reader: # read a row as {column1: value1, column2: value2,...}
        for (k,v) in row.items(): # go over each column name and value
            columns[k].append(v) # append the value into the appropriate list
                                 # based on column name k

columns['movie_title'] = [x.strip() for x in columns['movie_title']]
columns['movie_title'] = [x.strip('\xa0') for x in columns['movie_title']]


movieGenres = {}

for i in range(len(columns['movie_title'])):
    movieGenres[columns['movie_title'][i]] = columns['genres'][i]


movieNames = []
result = []
with open('mytweetsFormattedWithDecisions.txt', 'r') as input:
        lines = input.read().split("\n")

        for line in lines:
            lineparts = line.split("|")
            movieNames.append(lineparts[1])
            result.append(lineparts[2])



positiveReviews = 0
negatieReviews = 0
movieCalcNames = []
movieCalcPositive = []
movieCalcNegative = []
movieCalcGenres =[]

for i in range(len(columns['movie_title'])):
    for k in range(len(movieNames)):
        if movieNames[k] == columns['movie_title'][i]:
            if result [k] == "negativno":
                negatieReviews +=1
            else:
                positiveReviews +=1

    moviename = columns['movie_title'][i]
    movieGenres = columns['genres'][i]
    if(positiveReviews > 0 and negatieReviews > 0):

        movieCalcNames.append(moviename)

        movieCalcPositive.append(positiveReviews)

        movieCalcNegative.append(negatieReviews)

        movieCalcGenres.append(movieGenres)

    positiveReviews =0
    negatieReviews = 0



with open('moviedata.json', "w") as write_file:
    write_file.write("[\n")
    for i in range(len(movieCalcNames)):
        listGenres = str(movieCalcGenres[i]).split("|")
        data = {
            "movieName": movieCalcNames[i] ,
            "positiveReviews": movieCalcPositive[i],
            "negativeReviews": movieCalcNegative[i],
            "genres": listGenres
        }

        json.dump(data, write_file)
        if(i == len(movieCalcNames)-1):
            write_file.write("\n")
        else:
            write_file.write(",\n")
    write_file.write("]")