import math
import MovieGlobals

#
class IdSettings:
    def __init__(this):
        this.maxDepth = -1
        this.format = None

#
class IdNode:

    def __init__(this, idSettings):
        this.settings = idSettings
        this.index = -1
        this.mapping = []
        this.decision = None
        this.confidence = None

    def FindSplit(this, data, used):
        first = data[0]
        n = first.format.GetN()

        bestSplit = None
        bestSubsets = None
        bestEntropy = None

        for var in range(n):
            if used[var]:
                continue

            ssLib = []
            counts = []
            for sample in data:
                value = sample.values[var]
                while len(ssLib) <= value:
                    ssLib.append([])
                    counts.append(dict())
                
                ssLib[value].append(sample)
                
                thisCounts = counts[value]
                ans = sample.decision
                if ans in thisCounts:
                    thisCounts[ans] += 1
                else:
                    thisCounts[ans] = 1

            entropy = 0
            for i in range(len(ssLib)):
                ss = ssLib[i]
                appearances = counts[i]
                totalApp = len(ss)
                for cnt in appearances:
                    prob = appearances[cnt] / float(totalApp)
                    if prob > 0:
                        entropy += - prob * math.log2(prob)

            if bestSplit == None or entropy < bestEntropy:
                bestSplit = var
                bestSubsets = ssLib
                bestEntropy = entropy

        return bestSplit, bestSubsets

    def MostCommon(this, data):
        answers = dict()
        bestAns = None
        bestCnt = None
        for sample in data:
            ans = sample.decision
            if ans in answers:
                answers[ans] += 1
            else:
                answers[ans] = 1
            cnt = answers[ans]

            if bestAns == None or cnt > bestCnt:
                bestAns = ans
                bestCnt = cnt

        return bestAns, bestCnt / float(len(data))

    def Build(this, data, used, newDepth):
        this.depth = newDepth
        this.decision, this.confidence = this.MostCommon(data)
        
        # limit tree depth
        if newDepth == this.settings.maxDepth:
            return
        
        # split the tree
        bestIndex, bestMapping = this.FindSplit(data, used)
        this.index = bestIndex
        used[bestIndex] = True
        if MovieGlobals.DEBUG_ALLOWED:
            print(str(newDepth) + ":(" + ",".join(data[0].format.categories[bestIndex]) + "):(" + ",".join([str(len(ss)) for ss in bestMapping]) + ")")
        for i in range(len(bestMapping)):
            if len(bestMapping[i]) == 0:
                this.mapping.append(None)
            else:
                subTree = IdNode(this.settings)
                subTree.Build(bestMapping[i], used, newDepth + 1)
                this.mapping.append(subTree)
        used[bestIndex] = False

    def InitBuild(this, data):
        used = [False for i in range(this.settings.format.GetN())]
        this.Build(data, used, 0)

    def Find(this, data):
        #print("FB " + str(this.index) + "=" + str(data.values[this.index]))
        
        if this.index == -1:
            return this.decision, this.confidence
        value = min(data.values[this.index], len(this.mapping) - 1)
        subTree = this.mapping[value]
        if subTree == None:
            return this.decision, this.confidence
        return subTree.Find(data)














        
