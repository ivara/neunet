from MovieData import *
from IdNode import *
import MovieGlobals
import random

# returns the tweet in lower case without non-letter characters
def TweetFilter(strTweet):
    # lower case the letters
    strTweet = strTweet.lower()

    # prepare for filtering
    strFiltered = ""
    bAddSpace = False
    # for each character
    for i in range(len(strTweet)):
        # if letter, append it to filtered, mark next non-letter to add a space
        if 'a' <= strTweet[i] and strTweet[i] <= 'z':
            strFiltered += strTweet[i]
            bAddSpace = True
        # not a letter
        else:
            # consume the space marker if set and not and apostrophe (i've, don't etc.)
            if bAddSpace and strTweet[i] != "'":
                strFiltered += " "
                bAddSpace = False

    # return filtered string
    return strFiltered

# Handles training AI and evaluating tweets.
class IdDecisionForest:

    # Constructor.
    def __init__(this):
        this.andForest = []
        this.astrWords = []

    # Trains the AI on gives list of training files
    def Fit(this, astrTrainingFiles, iForestSize, iMaxDepth):
        this.andForest = []
        this.astrWords = []

        aastrTweetList = []

        # for each file
        for strInputFile in astrTrainingFiles:
            file = open(strInputFile, "r")
            astrLines = file.read().split("\n")
            # for each non-empty line
            for strLine in astrLines:
                if len(strLine) == 0:
                    continue

                # get the following: tweet, movie, evaluation
                astrParts = strLine.split("|")
                strTweet = TweetFilter(astrParts[0])

                # add it to the tweet list
                aastrTweetList.append(astrParts)

                # add all contained words to a dictionary
                astrTweetWords = strTweet.split(" ")
                for strWord in astrTweetWords:
                    if len(strWord) > 0:
                        bContained = False
                        for strExisting in this.astrWords:
                            if strExisting == strWord:
                                bContained = True
                        if not bContained:
                            this.astrWords.append(strWord)

        # required number of trees
        for iTree in range(iForestSize):
            iWordCount = len(this.astrWords)

            # determine the number of variables
            random.shuffle(this.astrWords)
            iVarCount = int((iWordCount + MovieGlobals.VARIABLE_V2_SIZE_PER_VAR - 1) / MovieGlobals.VARIABLE_V2_SIZE_PER_VAR)

            # start creating words format.
            wfFormat = WordsFormat(iVarCount)
            iWordsRemaining = iWordCount
            iNextWord = 0
            # for each variable
            for iVariable in range(iVarCount):
                if MovieGlobals.VARIABLE_V2_USE:
                    # take a number of words from the dictionary
                    iPhraseCount = min(iWordsRemaining, MovieGlobals.VARIABLE_V2_SIZE_PER_VAR)
                    iWordsRemaining -= iPhraseCount

                # for every phrase
                for j in range(iPhraseCount):
                    # add a word to the variable
                    wfFormat.AddWord(iVariable, this.astrWords[iNextWord])
                    iNextWord += 1

            if MovieGlobals.DEBUG_ALLOWED:
                print(" ".join([str(astr[0]) for astr in wfFormat.categories]))
                print(" ".join(this.astrWords))

            # create a training set
            awdTrainSet = []
            for astrTweet in aastrTweetList:
                #print("|".join(astrTweet))
                awdTrainSet.append(WordsData(wfFormat, TweetFilter(astrTweet[0]), astrTweet[2]))

            # create settings
            stgSettings = IdSettings()
            stgSettings.maxDepth = iMaxDepth
            stgSettings.format = wfFormat

            # create tree
            ndRoot = IdNode(stgSettings)
            ndRoot.InitBuild(awdTrainSet)
            this.andForest.append(ndRoot)

            # debug tree progress
            if MovieGlobals.REPORT_TREES_BUILT > 0 and (iTree + 1) % MovieGlobals.REPORT_TREES_BUILT == 0:
                print(str(iTree + 1) + " trees built.")

    # Evaluates given tweets and returns results.
    def Predict(this, astrTweets, fPower):
        astrResults = []
        afConfidences = []

        # for each tweet
        for strRaw in astrTweets:
            strTweet = TweetFilter(strRaw)
            fTotalConf = 0

            # for each tree and decision
            for ndRoot in this.andForest:
                wdData = WordsData(ndRoot.settings.format, strTweet, None)
                strDecision, fConf = ndRoot.Find(wdData)

                # calculate and add confidence differential
                fConfidenceDifferential = pow(fConf, fPower) - pow(1 - fConf, fPower)
                if strDecision == "pozitivno":
                    fTotalConf += fConfidenceDifferential
                else:
                    fTotalConf -= fConfidenceDifferential

            # final verdict - add to return list
            if fTotalConf > 0:
                astrResults.append("pozitivno")
                afConfidences.append(fTotalConf)
            else:
                astrResults.append("negativno")
                afConfidences.append(-fTotalConf)

        # return found answers
        return astrResults, afConfidences

def main():
    random.seed()
    #astrTrainingFiles = ["tp1.txt", "tp2.txt", "tp3.txt"]
    astrTrainingFiles = ["tp4.txt", "tp3.txt"]
    #astrTrainingFiles = ["tp2.txt"]
    #astrTrainingFiles = ["sp1.txt"]
    astrTestFiles = ["tp1.txt"]

    astrTweets = []
    astrAnswers = []
    for strTestFile in astrTestFiles:
        file = open(strTestFile, "r")
        astrLines = file.read().split("\n")
        for strLine in astrLines:
            if len(strLine) == 0:
                continue
            astrParts = strLine.split("|")
            astrTweets.append(astrParts[0])
            astrAnswers.append(astrParts[2])

    # kreiranje nove sume
    idfForest = IdDecisionForest()
    # treniranje UI: niz datoteka, velicina sume(20), maksimalna dubina(8)
    idfForest.Fit(astrTrainingFiles, MovieGlobals.FOREST_SIZE, MovieGlobals.MAX_DEPTH)
    # prima: (listu tweetova, strogocu(3)), vraca dva niza, bitan ti je samo prvi: niz ocjena (poz/neg)
    astrDecisions, afConf = idfForest.Predict(astrTweets, MovieGlobals.CONFIDENCE_SHARPNESS)

    aafMatrix = [[0, 0], [0, 0]]
    for i in range(len(astrTweets)):
        if astrAnswers[i] == "pozitivno":
            if astrDecisions[i] == "pozitivno":
                aafMatrix[0][0] += 1
            else:
                aafMatrix[0][1] += 1
        else:
            if astrDecisions[i] == "pozitivno":
                aafMatrix[1][0] += 1
            else:
                aafMatrix[1][1] += 1
    
    print("Total words: " + str(len(idfForest.astrWords)))
    print("\n".join([astrDecisions[i][0] + astrAnswers[i][0] + " " + str(afConf[i]) for i in range(len(afConf))]))
    print("\n".join([" ".join([str(f) for f in af]) for af in aafMatrix]))
    iCorrect = aafMatrix[0][0] + aafMatrix[1][1]
    iWrong = aafMatrix[0][1] + aafMatrix[1][0]
    print(str(iCorrect / float(iCorrect + iWrong)))

if __name__ == "__main__":
    main()
